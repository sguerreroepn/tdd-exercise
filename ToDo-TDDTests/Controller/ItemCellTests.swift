//
//  ItemCellTests.swift
//  ToDo-TDDTests
//
//  Created by Sebastian Guerrero F on 4/27/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import XCTest
@testable import ToDo_TDD

class ItemCellTests: XCTestCase {
  
  var tableView: UITableView!
  let dataSource = FakeDataSource()
  var cell: ItemCell!
  
  override func setUp() {
    super.setUp()
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let controller = storyboard
      .instantiateViewController(
        withIdentifier: "ItemListViewController")
      as! ItemListViewController
    _ = controller.view
    tableView = controller.tableView
    tableView?.dataSource = dataSource
    cell = tableView?.dequeueReusableCell(
      withIdentifier: "ItemCell",
      for: IndexPath(row: 0, section: 0)) as! ItemCell
  }
  
  func test_HasNameLabel() {
    XCTAssertNotNil(cell.titleLabel)
  }
  
  func test_HasNameLocationLabel() {
    XCTAssertNotNil(cell.locationLabel)
  }
  
  func test_ConfigCell_SetsTitle() {
    cell.configCell(with: ToDoItem(title: "Foo"))
    XCTAssertEqual(cell.titleLabel.text, "Foo")
  }
  
  func test_ConfigCell_SetsLabelTexts() {
    let location = Location(name: "Bar")
    let item = ToDoItem(title: "Foo",
                        itemDescription: nil,
                        timestamp: 1456150025,
                        location: location)
    cell.configCell(with: item)
    XCTAssertEqual(cell.titleLabel.text, "Foo")
    XCTAssertEqual(cell.locationLabel.text, "Bar")
    XCTAssertEqual(cell.dateLabel.text, "02/22/2016")
  }
  
  func test_Title_WhenItemIsChecked_IsStrokeThrough() {
    let location = Location(name: "Bar")
    let item = ToDoItem(title: "Foo",
                        itemDescription: nil,
                        timestamp: 1456150025,
                        location: location)
    cell.configCell(with: item, checked: true)
    let attributedString = NSMutableAttributedString(string: "Foo")
    attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributedString.length))
    XCTAssertEqual(cell.titleLabel.attributedText, attributedString)
    XCTAssertNil(cell.locationLabel.text)
    XCTAssertNil(cell.dateLabel.text)
  }
}


extension ItemCellTests {
  class FakeDataSource: NSObject, UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
      return 1
      
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
  }
  
}
