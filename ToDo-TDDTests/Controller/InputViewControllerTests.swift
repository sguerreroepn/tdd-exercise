//
//  InputViewControllerTests.swift
//  ToDo-TDDTests
//
//  Created by Sebastian Guerrero F on 4/28/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import XCTest
@testable import ToDo_TDD
import CoreLocation

class InputViewControllerTests: XCTestCase {
  
  var sut: InputViewController!
  var placemark: MockPlacemark!
  
  let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    return dateFormatter
  }()
  
  override func setUp() {
    super.setUp()
    let storyboard = UIStoryboard(name: "Main",
                                  bundle: nil)
    sut = storyboard.instantiateViewController(withIdentifier: "InputViewController") as! InputViewController
    
    _ = sut.view
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    sut.itemManager?.removeAll()
    sut.itemManager = nil
    super.tearDown()
  }
  
  func test_HasTitleTextField() {
    XCTAssertNotNil(sut.titleTextField)
  }
  
  func test_Save_UsesGeocoderToGetCoordinateFromAddress() {
    
    let mockInputViewController = MockInputViewController()
    mockInputViewController.titleTextField = UITextField()
    mockInputViewController.dateTextField = UITextField()
    mockInputViewController.locationTextField = UITextField()
    mockInputViewController.addressTextField = UITextField()
    mockInputViewController.descriptionTextField = UITextField()
    mockInputViewController.titleTextField.text = "Test Title"
    mockInputViewController.dateTextField.text = "02/22/2016"
    mockInputViewController.locationTextField.text = "Office"
    mockInputViewController.addressTextField.text = "Infinite Loop 1, Cupertino"
    mockInputViewController.descriptionTextField.text = "Test Description"
    let mockGeocoder = MockGeocoder()
    mockInputViewController.geocoder = mockGeocoder
    mockInputViewController.itemManager = ItemManager()
    let expectationDescription = expectation(description: "bla")
    mockInputViewController.completionHandler = {
      expectationDescription.fulfill()
    }
    let date = dateFormatter.date(from: mockInputViewController.dateTextField.text!)
    mockInputViewController.save()
    placemark = MockPlacemark()
    let coordinate = CLLocationCoordinate2DMake(37.3316851,
                                                -122.0300674)
    placemark.mockCoordinate = coordinate
    mockGeocoder.completionHandler?([placemark], nil)
    waitForExpectations(timeout: 1, handler: nil)
    let item = mockInputViewController.itemManager?.item(at: 0)
    let testItem = ToDoItem(title: "Test Title",
                            itemDescription: "Test Description",
                            timestamp: date?.timeIntervalSince1970,
                            location: Location(name: "Office", coordinate: coordinate))
    XCTAssertEqual(item, testItem)
  }
  
  func test_SaveButtonHasSaveAction() {
    let saveButton: UIButton = sut.saveButton
    guard let actions = saveButton.actions(
      forTarget: sut,
      forControlEvent: .touchUpInside) else {
        XCTFail(); return
    }
    XCTAssertTrue(actions.contains("save"))
  }
  
  func test_Geocoder_FetchesCoordinates() {
    let geocoderAnswered = expectation(description: "Geocoder")
    
    CLGeocoder().geocodeAddressString("Infinite Loop 1, Cupertino") {
      (placemarks, error) -> Void in
      let coordinate = placemarks?.first?.location?.coordinate
      guard let latitude = coordinate?.latitude else {
        XCTFail()
        return }
      guard let longitude = coordinate?.longitude else {
        XCTFail()
        return
      }
      XCTAssertEqual(latitude, 37.3316, accuracy: 0.001)
      XCTAssertEqual(longitude, -122.0300, accuracy: 0.001)
      geocoderAnswered.fulfill()
    }
    waitForExpectations(timeout: 3, handler: nil)
  }
  
  func test_Save_DismissesViewController() {
    let mockInputViewController = MockInputViewController()
    mockInputViewController.titleTextField = UITextField()
    mockInputViewController.dateTextField = UITextField()
    mockInputViewController.locationTextField = UITextField()
    mockInputViewController.addressTextField = UITextField()
    mockInputViewController.descriptionTextField = UITextField()
    mockInputViewController.titleTextField.text = "Test Title"
    mockInputViewController.save()
    XCTAssertTrue(mockInputViewController.dismissGotCalled)
  }
  
  func test_Cancel_DismissesViewController() {
    let mockInputViewController = MockInputViewController()
    mockInputViewController.cancel()
    XCTAssertTrue(mockInputViewController.dismissGotCalled)
  }
}

extension InputViewControllerTests {
  class MockGeocoder: CLGeocoder {
    var completionHandler: CLGeocodeCompletionHandler?
    override func geocodeAddressString(
      _ addressString: String,
      completionHandler: @escaping CLGeocodeCompletionHandler) {
      self.completionHandler = completionHandler
    }
  }
  
  class MockPlacemark : CLPlacemark {
    var mockCoordinate: CLLocationCoordinate2D?
    override var location: CLLocation? {
      guard let coordinate = mockCoordinate else
      { return CLLocation() }
      return CLLocation(latitude: coordinate.latitude,
                        longitude: coordinate.longitude)
    }
  }
  
  class MockInputViewController : InputViewController {
    
    var dismissGotCalled = false
    var completionHandler: (() -> Void)?
    
    override func dismiss(animated flag: Bool,
                          completion: (() -> Void)? = nil) {
      dismissGotCalled = true
      completionHandler?()
    }
  }
  
}
