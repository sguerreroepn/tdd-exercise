//
//  ItemListViewControllerTests.swift
//  ToDo-TDDTests
//
//  Created by Sebastian Guerrero F on 4/27/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import XCTest
import UIKit
@testable import ToDo_TDD

class ItemListViewControllerTests: XCTestCase {
  
  var sut: ItemListViewController!
  var action: Selector!
  var addButton: UIBarButtonItem!
  
  override func setUp() {
    super.setUp()
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    sut = storyboard.instantiateViewController(withIdentifier: "ItemListViewController")
      as! ItemListViewController
    guard let addBarButton = sut.navigationItem.rightBarButtonItem else
    { XCTFail(); return }
    addButton = addBarButton
    guard let barButtonAction = addButton?.action else { XCTFail(); return }
    action = barButtonAction
    UIApplication.shared.keyWindow?.rootViewController = sut
    
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func test_TableView_AfterViewDidLoad_IsNotNil() {
    XCTAssertNotNil(sut.tableView)
  }
  
  func test_LoadingView_SetsTableViewDataSource() {
    XCTAssertTrue(sut.tableView.dataSource is ItemListDataProvider)
  }
  
  func test_LoadingView_SetsTableViewDelegate() {
    XCTAssertTrue(sut.tableView.delegate is ItemListDataProvider)
  }
  
  func test_LoadingView_SetsDataSourceAndDelegateToSameObject() {
    XCTAssertEqual(sut.tableView.dataSource as? ItemListDataProvider,
                   sut.tableView.delegate as? ItemListDataProvider)
  }
  
  func test_ItemListViewController_HasAddBarButtonWithSelfAsTarget() {
    let target = sut.navigationItem.rightBarButtonItem?.target
    XCTAssertEqual(target as? UIViewController, sut)
  }
  
  func test_AddItem_PresentsAddItemViewController() {
    XCTAssertNil(sut.presentedViewController)
    sut.performSelector(onMainThread: action,
                        with: addButton,
                        waitUntilDone: true)
    XCTAssertNotNil(sut.presentedViewController)
    XCTAssertTrue(sut.presentedViewController is InputViewController)
    let inputViewController =
      sut.presentedViewController as! InputViewController
    XCTAssertNotNil(inputViewController.titleTextField)
  }
  
  func test_ViewWillAppear_ReloadsData() {
    let mockTableView = MockTableView()
    sut.tableView = mockTableView
    sut.viewWillAppear(true)
    XCTAssertTrue(mockTableView.didReloadData)
  }
  
  
  func testItemListVC_SharesItemManagerWithInputVC() {
    sut.performSelector(onMainThread: action,
                        with: addButton,
                        waitUntilDone: true)
    guard let inputViewController =
      sut.presentedViewController as? InputViewController else
    { XCTFail(); return }
    guard let inputItemManager = inputViewController.itemManager else
    { XCTFail(); return }
    XCTAssertTrue(sut.itemManager === inputItemManager)
  }
  
  func test_ViewDidLoad_SetsItemManagerToDataProvider() {
    XCTAssertTrue(sut.itemManager === sut.dataProvider.itemManager)
  }
}

extension ItemListViewControllerTests {
  
  class MockTableView:UITableView {
    var didReloadData = false
    override func reloadData() {
      didReloadData = true
    }
  }
}
