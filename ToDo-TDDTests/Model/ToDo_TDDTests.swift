//
//  ToDo_TDDTests.swift
//  ToDo-TDDTests
//
//  Created by Sebastian Guerrero F on 4/26/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import XCTest
@testable import ToDo_TDD

class ToDo_TDDTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func test_Init_TakesTitle() {
    let item = ToDoItem(title: "Test title")
    XCTAssertNotNil(item, "item should not be nil")
  }
  
  func test_Init_TakesTitleAndDescription() {
    _ = ToDoItem(title: "Test title", itemDescription: "Test description")
  }
  
  func test_Init_SetsTitle() {
    let item = ToDoItem(title: "Foo")
    XCTAssertEqual(item.title, "Foo", "should set title")
  }
  
  func test_Init_WhenGivingDescription_SetsDescription() {
    let item = ToDoItem(title: "Test title", itemDescription: "Bar")
    XCTAssertEqual(item.itemDescription, "Bar", "should set title")
  }
  
  func test_Init_WhenGivenTimestamp_SetsTimestamp() {
    let item = ToDoItem(title: "", timestamp: 0.0)
    XCTAssertEqual(item.timestamp, 0.0, "should set timestamp")
  }
  
  func test_Init_WhenGivenLocation_SetsLocation() {
    let location = Location(name: "Foo")
    XCTAssertEqual(location.name,"Foo", "should set name")
    let item = ToDoItem(title: "", location: location)
    XCTAssertEqual(item.location?.name, location.name,
                   "should set location")
  }
  
  func test_EqualItems_AreEqual() {
    let first = ToDoItem(title: "Foo")
    let second = ToDoItem(title: "Foo")
    XCTAssertEqual(first, second)
  }
  
  func test_Items_WhenLocationDiffers_AreNotEqual() {
    let first = ToDoItem(title: "", location: Location(name: "Foo"))
    let second = ToDoItem(title: "", location: Location(name: "Bar"))
    XCTAssertNotEqual(first, second)
  }
  
  func test_Items_WhenTimestampsDiffer_AreNotEqual() {
    let first = ToDoItem(title: "Foo", timestamp: 1.0)
    let second = ToDoItem(title: "Foo", timestamp: 0.0)
    XCTAssertNotEqual(first, second)
  }
  
  func test_Items_WhenDescriptionsDiffer_AreNotEqual() {
    let first = ToDoItem(title: "Foo", itemDescription: "Bar")
    let second = ToDoItem(title: "Foo", itemDescription: "Baz")
    XCTAssertNotEqual(first, second)
  }
  
  func test_Items_WhenTitlesDiffer_AreNotEqual() {
    let first = ToDoItem(title: "Foo")
    let second = ToDoItem(title: "Bar")
    XCTAssertNotEqual(first, second)
  }
  
  func test_HasPlistDictionaryProperty() {
    let item = ToDoItem(title: "First")
    let dictionary = item.plistDict
    XCTAssertNotNil(dictionary)
  }
  
  func test_CanBeCreatedFromPlistDictionary() {
    let location = Location(name: "Home")
    let item = ToDoItem(title: "The Title",
                        itemDescription: "The Description",
                        timestamp: 1.0,
                        location: location)
    let dict = item.plistDict
    let recreatedItem = ToDoItem(dict: dict)
    XCTAssertEqual(item, recreatedItem)
  }
  
  
}
