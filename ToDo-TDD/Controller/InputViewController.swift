//
//  InputViewController.swift
//  ToDo-TDD
//
//  Created by Sebastian Guerrero F on 4/30/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit
import CoreLocation

class InputViewController: UIViewController {
  
  @IBOutlet weak var titleTextField: UITextField!
  @IBOutlet weak var descriptionTextField: UITextField!
  @IBOutlet weak var dateTextField: UITextField!
  @IBOutlet weak var locationTextField: UITextField!
  @IBOutlet weak var addressTextField: UITextField!
  @IBOutlet weak var saveButton: UIButton!
  
  
  lazy var geocoder = CLGeocoder()
  var itemManager: ItemManager?
  
  let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    return dateFormatter
  }()
  
  @IBAction func save() {
    guard let titleString = titleTextField.text,
      titleString.count > 0 else { return }
    let date: Date?
    if let dateText = self.dateTextField.text,
      dateText.count > 0 {
      date = dateFormatter.date(from: dateText)
    } else {
      date = nil }
    let descriptionString = descriptionTextField.text
    if let locationName = locationTextField.text,
      locationName.count > 0 {
      if let address = addressTextField.text,
        address.count > 0 {
        geocoder.geocodeAddressString(address) {
          [unowned self] (placeMarks, error) -> Void in
          let placeMark = placeMarks?.first
          let item = ToDoItem(
            title: titleString,
            itemDescription: descriptionString,
            timestamp: date?.timeIntervalSince1970,
            location: Location(
              name: locationName,
              coordinate: placeMark?.location?.coordinate))
          DispatchQueue.main.async {
            self.itemManager?.add(item)
            self.dismiss(animated: true, completion: nil)
          }
        }
      }
    } else {
      let item = ToDoItem(title: titleString,
                          itemDescription: descriptionString,
                          timestamp: date?.timeIntervalSince1970,
                          location: nil)
      self.itemManager?.add(item)
      self.dismiss(animated: true, completion: nil)
    }
    
  }
  @IBAction func cancel() {
    dismiss(animated: true, completion: nil)
  }
}
