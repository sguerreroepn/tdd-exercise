//
//  ItemManager.swift
//  ToDo-TDD
//
//  Created by Sebastian Guerrero F on 4/27/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

class ItemManager: NSObject{
  var toDoCount:Int { return toDoItems.count }
  var doneCount:Int { return doneItems.count }
  private var toDoItems: [ToDoItem] = []
  private var doneItems: [ToDoItem] = []
  
  var toDoPathURL: URL {
    let fileURLs = FileManager.default.urls(
      for: .documentDirectory, in: .userDomainMask)
    guard let documentURL = fileURLs.first else {
      print("Something went wrong. Documents url could not be found")
        fatalError()
    }
    return documentURL.appendingPathComponent("toDoItems.plist")
  }
  
  var donePathURL: URL {
    let fileURLs = FileManager.default.urls(
      for: .documentDirectory, in: .userDomainMask)
    guard let documentURL = fileURLs.first else {
      print("Something went wrong. Documents url could not be found")
      fatalError()
    }
    return documentURL.appendingPathComponent("doneItems.plist")
  }

  override init() {
    super.init()
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(save),
      name: NSNotification.Name.UIApplicationWillResignActive,
      object: nil)
    
    if let nsToDoItems = NSArray(contentsOf: toDoPathURL) {
      for dict in nsToDoItems {
        if let toDoItem = ToDoItem(dict: dict as! NSDictionary) {
          toDoItems.append(toDoItem)
        }
      }
    }
    if let nsDoneItems = NSArray(contentsOf: donePathURL) {
      for dict in nsDoneItems {
        if let doneItem = ToDoItem(dict: dict as! NSDictionary) {
          doneItems.append(doneItem)
        }
      }
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
    save()
  }
  
  func add(_ item:ToDoItem) {
    if !toDoItems.contains(item) {
      toDoItems += [item]
    }
  }
  
  func item(at index:Int) -> ToDoItem {
    return toDoItems[index]
  }
  
  func checkItem(at index:Int) {
    let checkedItem = toDoItems.remove(at: index)
    doneItems += [checkedItem]
  }
  
  func uncheckItem(at index: Int) {
    let item = doneItems.remove(at: index)
    toDoItems.append(item)
  }
  
  func doneItem(at index:Int) -> ToDoItem {
    return doneItems[index]
  }
  
  func removeAll() {
    toDoItems.removeAll()
    doneItems.removeAll()
  }
  
  
  @objc func save() {
    //save ToDo items
    var nsToDoItems = [AnyObject]()
    for item in toDoItems {
      nsToDoItems.append(item.plistDict)
    }
    if nsToDoItems.count > 0 {
      (nsToDoItems as NSArray).write(to: toDoPathURL,
                                          atomically: true)
    } else {
      do {
        try FileManager.default.removeItem(at: toDoPathURL)
      } catch {
        print(error)
      }
    }
    
    //save done Items
    var nsDoneItems = [AnyObject]()
    for item in doneItems {
      nsDoneItems.append(item.plistDict)
    }
    if nsDoneItems.count > 0 {
      (nsDoneItems as NSArray).write(to: donePathURL,
                                     atomically: true)
    } else {
      do {
        try FileManager.default.removeItem(at: donePathURL)
      } catch {
        print(error)
      }
    }
  }
}
